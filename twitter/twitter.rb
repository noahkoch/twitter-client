require 'oauth'
require 'yaml'
require 'rest-client'
require 'launchy'
require 'json'

class TwitterSession
  CONSUMER_KEY = "tfIZ0KFaGQrVJEYOBE4KfA"
  CONSUMER_SECRET = "4scvrnZRk6ZtnHL0l379VgT3qiY8F8fEdO8dcM9XZk"
  CONSUMER = OAuth::Consumer.new(
    CONSUMER_KEY, CONSUMER_SECRET, :site => "https://twitter.com"
  )

  def self.access_token
    request_token = CONSUMER.get_request_token

    authorize_url = request_token.authorize_url
    Launchy.open(authorize_url)
    puts "enter code"
    oauth_verifier = gets.chomp
    @@access_token = request_token.get_access_token(
      :oauth_verifier => oauth_verifier
    )
  end

  def self.get_or_save_token(token_file)
    if File.exist?(token_file)
      File.open(token_file){|f| YAML.load(f)}
    else
      @@access_token = TwitterSession.access_token
      File.open(token_file, "w") {|f| YAML.dump(@@access_token, f)}

      @@access_token
    end
  end

end

class Status

  attr_accessor :text, :user

  def initialize(author, message)
    @user = author
    @text = message
  end

  def self.parse(json)
    JSON.parse(json)
  end

end

class User

  attr_accessor :username

  def initialize(username)
    @username = username
  end

  def timeline
    Status.parse(TwitterSession.get_or_save_token(username).get("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=noahkoch&count=1").body)
  end

  def followers
    User.parse_many(TwitterSession.get_or_save_token(username).get("https://api.twitter.com/1.1/followers/ids.json?cursor=-1&screen_name=noahkoch&count=10").body)
  end

  def followed_users
    User.parse_many(TwitterSession.get_or_save_token(username).get("https://api.twitter.com/1.1/followers/ids.json?cursor=-1&screen_name=noahkoch&count=10").body)
  end

  def self.parse(json)
    hash = JSON.parse(json)
    User.new(hash[0]['screen_name'])
  end

  def self.parse_many(json)
    hash = JSON.parse(json)
    ids = hash['ids']
    user_array = []
    ids.each do |id|
      user_array << User.parse(TwitterSession.get_or_save_token("NoahKoch").get("https://api.twitter.com/1.1/users/lookup.json?user_id=#{id}").body)
    end
    user_array
  end
end

class EndUser < User


  def initialize(user_name)
    super(user_name)
  end

  def self.set_user_name(user_name)
    @@current_user = EndUser.new(user_name)
  end

  def self.me
    @@current_user
  end

  def post_status(status_text)
    TwitterSession.get_or_save_token(username).post("https://api.twitter.com/1.1/statuses/update.json?status=#{URI.escape(status_text)}")
  end

  def direct_message(other_user, text)
    TwitterSession.get_or_save_token(username).post("https://api.twitter.com/1.1/direct_messages/new.json?screen_name=#{other_user}&text=#{URI.escape(text)}")
  end

end